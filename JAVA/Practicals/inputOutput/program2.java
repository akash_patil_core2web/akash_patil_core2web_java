// Write a program to check whether the given 
// number is prime or not.


import java.io.*;
class program2 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a number :");
                int num=Integer.parseInt(br.readLine());

                int temp=1;
		int count=0;

                while(temp<=num) {
                        if(num%temp==0) {
                                count++;
                        }
                        temp++;
                }

		if(count==2) {
			System.out.println(num+" is prime number");

		}
		else {
			System.out.println(num+" is not prime number");
		}

        }
}
