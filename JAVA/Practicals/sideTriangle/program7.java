/* row = 3
 *     1
 *   2 1
 * 3 2 1
 *   2 1
 *     1
 *
 * row =4
 *       1
 *     2 1
 *   3 2 1
 * 4 3 2 1
 *   3 2 1
 *     2 1
 *       1	*/

import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<row*2;i++) {
			int num=row*2-i;
                        if(i<=row) {
                                for(int j=row;j>i;j--) {
                                        System.out.print("\t");
                                }
                                for(int j=i;j>=1;j--) {
                                        System.out.print(j +"\t");
                                }
                        }
                        else {
                                for(int j=row;j<i;j++) {
                                        System.out.print("\t");
                                }
                                for(int j=1;j<=row*2-i;j++) {
                                        System.out.print(num-- +"\t");
                                 }
                         }                                                                                                                                                          System.out.println();
                }
        }
}
