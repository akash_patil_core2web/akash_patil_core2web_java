/* row =3
 * 3
 * 2 2
 * 1 1 1
 * 2 2
 * 3
 *
 * row = 4
 * 4
 * 3 3
 * 2 2 2
 * 1 1 1 1
 * 2 2 2
 * 3 3
 * 4    */

import java.io.*;
class program4a {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		int num=row;
		int num1=2;
                for(int i=1;i<row*2;i++) {
                        if(i<=row) {
                                for(int j=1;j<=i;j++) {
                                        System.out.print(num +" ");
                                }
				num--;
                        }

                        else {
                                for(int j=1;j<=row*2-i;j++) {
                                        System.out.print(num1 +" ");
                                }
				num1++;
                        }
                        System.out.println();
                }
        }
}
