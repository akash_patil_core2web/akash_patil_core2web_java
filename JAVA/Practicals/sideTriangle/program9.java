 /* row =4 
 *       3
 *     2 3
 *   1 2 3
 * 0 1 2 3
 *   1 2 3 
 *     2 3
 *       3
 *
 * row = 3
 *     2
 *   1 2
 * 0 1 2
 *   1 2
 *     2	*/ 


import java.io.*;
class program9 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		
                for(int i=1;i<row*2;i++) {
                        int num=row-i;
			int num1=0;
                        if(i<=row) {
                                for(int j=row;j>i;j--) {
                                        System.out.print("\t");
                                }
                                for(int j=i;j>=1;j--) {
                                        System.out.print(num++ +"\t");
                                }
                        }
                        else {
                                for(int j=row;j<i;j++) {
                                        System.out.print("\t");
					num1++;
                                }
                                for(int j=1;j<=row*2-i;j++) {
                                        System.out.print(num1++ +"\t");
                                 }
			}                                                                                                                                                          System.out.println();
                }
        }
}
