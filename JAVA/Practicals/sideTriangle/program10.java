/* row = 3
 *     1C
 *   B C
 * A B C
 *   B C
 *     C
 *
 * ROW = 4
 *
 *       D
 *     C D
 *   B C D
 * A B C D
 *   B C D
 *     C D
 *       D	*/

import java.io.*;
class program10 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<row*2;i++) {
                        int num=(row-i)+65;
                        int num1=65;
                        if(i<=row) {
                                for(int j=row;j>i;j--) {
                                        System.out.print("\t");
                                }
                                for(int j=i;j>=1;j--) {
                                        System.out.print((char)num +"\t");
					num++;
                                }
                        }
                        else {
                                for(int j=row;j<i;j++) {
                                        System.out.print("\t");
                                        num1++;
                                }
                                for(int j=1;j<=row*2-i;j++) {
                                        System.out.print((char)num1 +"\t");
					num1++;
                                 }
                         }                                                                                                                                                          System.out.println();
                }
        }
}
