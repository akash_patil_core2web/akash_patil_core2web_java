/* row = 3
 * #
 * # #
 * # # #
 * # #
 * #
 *
 * row = 4
 * #
 * # #
 * # # #
 * # # # #
 * # # #
 * # #
 * #    */
import java.io.*;
class program2b {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		
		int num=row;
                for(int i=1;i<row*2;i++) {
                        if(i<=row) {
                                for(int j=1;j<=i;j++) {
                                        System.out.print(" # ");
                                }
                        }

                        else {
                                for(int j=1;j<=num;j++) {
                                        System.out.print(" # ");
					
                                }
                        }
                        System.out.println();
                }
        }
}
