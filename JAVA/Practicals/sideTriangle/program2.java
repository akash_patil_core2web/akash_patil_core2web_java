/* row = 3
 * #
 * # #
 * # # #
 * # #
 * #
 *
 * row = 4
 * #
 * # #
 * # # #
 * # # # #
 * # # #
 * # # 
 * #	*/
import java.io.*;
class program2 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++) {
                        for(int j=1;j<=i;j++) {
                                System.out.print(" # ");
                        }
                        System.out.println();
                }
                for(int i=1;i<row;i++) {
                        for(int j=1;j<=row-i;j++) {
                                System.out.print(" # ");
                        }
                        System.out.println();
                }
        }
}
