/* row =3
 * C
 * B B 
 * A A A 
 * B B
 * C
 *
 * row = 4
 * D
 * C C
 * B B B
 * A A A A
 * B B B
 * C C
 * D    */

import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
                int num=row+64;
                int num1=2+64;
                for(int i=1;i<row*2;i++) {
                        if(i<=row) {
                                for(int j=1;j<=i;j++) {
                                        System.out.print((char)num +" ");
                                }
                                num--;
                        }

                        else {
                                for(int j=1;j<=row*2-i;j++) {
                                        System.out.print((char)num1 +" ");
                                }
                                num1++;
                        }
                        System.out.println();
                }
	}
}
