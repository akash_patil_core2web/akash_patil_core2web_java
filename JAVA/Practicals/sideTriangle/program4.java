/* row =3
 * 3
 * 2 2
 * 1 1 1
 * 2 2
 * 3
 *
 * row = 4
 * 4
 * 3 3 
 * 2 2 2
 * 1 1 1 1
 * 2 2 2
 * 3 3
 * 4	*/
import java.io.*;
class program4 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		int num=row;
                for(int i=1;i<=row;i++) {
                        
                        for(int j=1;j<=i;j++) {
                                System.out.print(num +" ");
                        }
			num--;
                        System.out.println();
                }
                for(int i=1;i<row;i++) {

                        for(int j=row-i;j>=1;j--) {
                                System.out.print(i+1 +" ");
                        }
                        System.out.println();
                }
        }
}
