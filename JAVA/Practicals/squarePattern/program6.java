/* row = 3
 *
 * 9  4  25
 * 6  49 8
 * 81 10 121
 *
 * Row = 4
 * 4  25  6  49
 * 8  81  10 121
 * 12 169 14 225
 * 16 289 18 361    */

import java.io.*;
class program6 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(num%2==1) {
                                        System.out.print(num*num +" ");
                                }
                                else {
                                        System.out.print(num +" ");
                                }
                                num++;
                        }
                        System.out.println();
                }
        }
}
