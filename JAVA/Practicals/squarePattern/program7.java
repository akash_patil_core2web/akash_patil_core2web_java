/* row = 3
 *
 * A  4  A
 * 6  B  8
 * C  10 C
 *
 * Row = 4
 * 4  A  6  A
 * 8  B  10 B
 * 12 C  14 C
 * 16 D  18 D     */

import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
		char ch='A';
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(num%2==1) {
                                        System.out.print(ch +" ");
                                }
                                else {
                                        System.out.print(num +" ");
                                }
                                
				num++;
                        }
			ch++;
                        System.out.println();
                }
        }
}
