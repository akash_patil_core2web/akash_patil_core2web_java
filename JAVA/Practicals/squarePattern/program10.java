/*Row = 3
 *3  B  1
  C  B  A
  3  B  1
 
 * Row = 4
 * 4  C  2  A
 * D  C  B  A
 * 4  C  2  A
 * D  C  B  A	*/

import java.io.*;
class program10 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
                for(int i=1;i<=row;i++) {
			int num = row;
			int ch = 64+row;
                        for(int j=1;j<=row;j++) {
				if(i%2==1) {
                                	if(j%2==1) {
						System.out.print(num +" ");
                        		}
					else {
						System.out.print((char)(ch) +" ");
					}
					num--;
					ch--;
				}
				else {
				
                                        System.out.print((char)(ch) +" ");
					ch--;
				}
			}
                        System.out.println();
                }
        }
}
