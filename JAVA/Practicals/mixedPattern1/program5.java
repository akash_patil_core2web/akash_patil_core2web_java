/* row = 3
 *
 * 9  16  25
 * 6  7   8
 * 81 100 121
 *
 * Row = 4
 * 9   16   25   36
 * 7   8    9    10
 * 121 144  169  225
 * 16  17   18   19	*/

import java.io.*;
class program5 {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows :");
		int row = Integer.parseInt(br.readLine());
		int num=3;
		for(int i=1;i<=row;i++) {
			
			for(int j=1;j<=row;j++) {
				if(i%2==1) {
					System.out.print(num*num +"\t");
					
				}
				else {
					System.out.print(num +"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}


