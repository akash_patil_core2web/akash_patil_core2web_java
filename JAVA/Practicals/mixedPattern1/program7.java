/* row =3 
 * 		 1
 *             3 2 1
 *           5 4 3 2 1
 *
 * row = 4
 *
 * 		 1
 * 	       3 2 1 
 *           5 4 3 2 1
 * 	   7 6 5 4 3 2 1	*/

import java.io.*;
class program7 {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows :");
		int row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
			}
			
			for(int j=1;j<=i*2-1;j++) {
				System.out.print(num-- +" ");
			}
			num=i*2+1;
			System.out.println();
		}
	}
}

