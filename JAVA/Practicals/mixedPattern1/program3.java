/*Row = 3
 *           1
 *        4  7
 *     10 13 16
 *
 *Row = 4
             1
          5  9
      13  17 21
  25 29  33  37 */


import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
                int num = 1;
		int num1=row;
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++){
                                System.out.print("\t");
                        }


                        for(int j=1;j<=i;j++) {
                                System.out.print(num +"\t");
                                num+=num1;
                        }
                        System.out.println();
                }
        }
}
