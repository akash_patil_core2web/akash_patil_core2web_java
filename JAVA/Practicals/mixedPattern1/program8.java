/* row =3
 *               3
 *             2 3 4
 *           1 2 3 4 5
 *
 * row = 4
 *
 *               4
 *             3 4 5
 *           2 3 4 5 6
 *         1 2 3 4 5 6 7        */

import java.io.*;
class program8 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
                int num = row;
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++) {
                                System.out.print("  ");
                        }

                        for(int j=1;j<=i*2-1;j++) {
                                System.out.print(num++ +" ");
                        }
                        num=row-i;
                        System.out.println();
                }
        }
}
