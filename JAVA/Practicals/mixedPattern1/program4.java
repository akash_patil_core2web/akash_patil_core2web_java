/*  row= 4
 *
 * a b c d
 *   b c d
 *     c d
 *       d
 *
 *  row = 3
 *
 *  A B C
 *    B C
 *      C       */

import java.io.*;
class program4
{
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<i;space++) {
                                System.out.print("\t");
                        }
                        for(int j=i;j<=row;j++) {
                                if(num%2==0) {
					System.out.print((char)(96+j) +"\t");
				}
				else {
					System.out.print((char)(64+j) +"\t");

				}
                        }
                        
                        System.out.println();
                }
        }
}
