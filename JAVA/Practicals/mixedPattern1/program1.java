/*Row = 3
 * 	     1
 *        3  5
 *      7 9 11
 *
 *Row = 4
 	     1 
	  3  5
      7   9 11
   13 17 19 23	*/


import java.io.*;
class program1 {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows : ");
		int row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++){
				System.out.print("\t");
			}     

			
			for(int j=1;j<=i;j++) {
				System.out.print(num +"\t");
				num+=2;
			}
			System.out.println();
		}
	}
}



