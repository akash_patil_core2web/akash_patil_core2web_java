/*  row= 4
 *
 * 1 2 3 4
 *   4 5 6
 *     6 7
 *       7
 *
 *  row = 3
 *
 *  1 2 3 
 *    3 4
 *      4	*/

import java.io.*;
class program2 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
		
		int num=1;
		for(int i=1;i<=row;i++) {
			for(int space=1;space<i;space++) {
				System.out.print("  ");
			}
			for(int j=i;j<=row;j++) {
				System.out.print(num++ +" ");
			}
			num-=1;
			System.out.println();
		}
	}
}


