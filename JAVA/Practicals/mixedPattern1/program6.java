/* row =3 
 * 		 1
 *             1 2 3
 *           1 2 3 4 5
 *
 * row = 4
 *
 * 		 1
 * 	       1 2 3 
 *           1 2 3 4 5
 * 	   1 2 3 4 5 6 7	*/

import java.io.*;
class program6 {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows :");
		int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
			}
			int num=1;
			for(int j=1;j<=i*2-1;j++) {
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

