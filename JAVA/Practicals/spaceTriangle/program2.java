/*row = 3
 * 	   3
 *  	 3 2
 *     3 2 1
 *
 *row = 4

 	     4
	  4  3
       4  3  2
     4 3  2  1	*/

import java.io.*;
class program2 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
			}
			int num=row;
			for(int j=1;j<=i;j++) {
				System.out.print(num-- +" ");
			}
			System.out.println();
		}
	}
}
