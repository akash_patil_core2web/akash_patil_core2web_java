/* row = 3
 *
 * 1 2 3
 *   1 2
 *     1
 *
 * row = 4
 *
 * 1 2 3 4
 *   1 2 3 
 *     1 2
 *       1	*/

import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++) {
			for(int space=1;space<i;space++) {
				System.out.print("  ");
			}
			int num=1;
			for(int j=i;j<=row;j++) {
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
