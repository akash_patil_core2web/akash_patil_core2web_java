/* row =3
 *
 * 	    C
 * 	  B C
 * 	A B C
 *
 * ROW = 4
 *
 * 	    D
 * 	  C D
 * 	B C D
 *   A  B C D	*/

import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());

		//char ch='A';

		for(int i=1;i<=row;i++) {
			char ch = 'A';
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
				ch++;
			}
			for(int j=1;j<=i;j++) {
				System.out.print(ch++ +" ");
			}
			System.out.println();
		}
	}
}
