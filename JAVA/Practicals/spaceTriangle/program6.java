/*  row= 4
 *
 * 4 4 4 4
 *   3 3 3
 *     2 2
 *       1
 *
 *  row = 3
 *
 *  3 3 3 
 *    2 2
 *      1	*/

import java.io.*;
class program6 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
		
		int num=row;
		for(int i=1;i<=row;i++) {
			for(int space=1;space<i;space++) {
				System.out.print("  ");
			}
			for(int j=row;j>=i;j--) {
				System.out.print(num+" ");
			}
			num--;
			System.out.println();
		}
	}
}

