/* row =3 
 * 	     3
 * 	   3 6
 * 	 3 6 9
 *
 * row = 4
 *
 * 	     4
 * 	   4 8
 * 	4 8 12
 *    4 8 12 16	 */

import java.io.*;
class program4 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++) {
				System.out.print(row*j+" ");
			}
			System.out.println();
		}
	}
}

