/* row = 3
 *
 * c
 * 3 2
 * c b a
 *
 * row = 4
 *
 * d
 * 4 3
 * d c b
 * 4 3 2 1	*/

import java.io.*;
class program6 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++){
			int num=row;
			int ch=96+row;
                        for(int j=1;j<=i;j++){
				if(i%2==1){
                                	System.out.print((char)ch-- +" ");
                        	}
				else {
					System.out.print(num-- +" ");
				}
                       
			}
			System.out.println();
                }
        }
}
