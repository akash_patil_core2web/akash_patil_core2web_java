/* row =4 
 *
 * 1 2 3 4 
 * C B A
 * 1 2
 * A
 *
 * row = 5
 *
 * 1 2 3 4 5
 * D C B A
 * 1 2 3
 * B A 
 * 1	*/

import java.io.*;
class program9 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());
		
                //int ch=64+row-i+1;
                for(int i=1;i<=row;i++){
			int ch = 64+row-i+1;
                        for(int j=1;j<=row-i+1;j++){
				if(i%2==1) {
                                	System.out.print(j +" ");
                                	
				}
				else {
					System.out.print((char)ch-- +" ");
				}
                        }
			
                        System.out.println();
                }
        }
}
