/* row = 3
 *
 * 3
 * 2 4
 * 1 2 3
 *
 * row = 4
 *
 * 4
 * 3 6
 * 2 4 6
 * 1 2 3 4	*/

import java.io.*;
class program4 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++){
                        for(int j=1;j<=i;j++){
                                System.out.print(num*j +" ");
				
                        }
			num=row-i;
                        System.out.println();
                }
        }
}
