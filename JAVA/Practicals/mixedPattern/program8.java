/*row = 3
 * F E D
 * C B
 * A
 *
 * row = 4
 *
 * J I H G
 * F E D
 * C B
 * A		*/

import java.io.*;
class program8 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());

                int ch=(row*(row+1))/2;
                for(int i=1;i<=row;i++){
                        for(int j=row;j>=i;j--){
                                System.out.print((char)(ch+64) +" ");
				ch--;
                        }
                        System.out.println();
                }
        }
}
