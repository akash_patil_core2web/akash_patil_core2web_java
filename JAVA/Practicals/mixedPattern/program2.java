/* row = 3
 *
 * C3 C2 C1
 * C4 C3 C2
 * C5 C4 C3
 *
 * row = 4
 *
 * D4 D3 D2 D1
 * D5 D4 D3 D2
 * D6 D5 D4 D3
 * D7 D6 D5 D4	*/

import java.io.*;
class program2 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
		int ch=64+row;
                for(int i=1;i<=row;i++){
			
                        for(int j=1;j<=row;j++){
                                System.out.print(""+ (char)ch + num-- +" ");
                        }
			num=row+i;
                        System.out.println();
                }
        }
}
