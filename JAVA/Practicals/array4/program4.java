//number occurs 2 times or more than 2 times
import java.io.*;
class program4 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
		System.out.print("Enter no to check :");
		int num = Integer.parseInt(br.readLine());
		int count = 0;
                 for (int i = 0; i<size; i++) {
                        if(arr[i]==num) {
                                count++;
                        }
		 }
                 if(count == 2) {
                        System.out.print(num+" occurs 2 times in array ");
                 }
		 else if(count > 2) {
                        System.out.print(num+" occurs more than 2 times in array ");
                        }
		 else {
                        System.out.print(num+" occurs less than 2 times in array ");
                  }
                    
        }
}
