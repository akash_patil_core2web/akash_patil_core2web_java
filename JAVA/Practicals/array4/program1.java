// Print average of array elements

import java.io.*;
class program1 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                 int add = 0;
                 for (int i = 0; i<size; i++) {
                 	add = add + arr[i];
                    } 
                System.out.println("Array elements average is : "+add/size);
        }
}
