// reverse the array
import java.io.*;
class program5 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                 int temp =0;
                 for (int i = 0; i<size/2; i++) {
                        temp = arr[i];
			arr[i]=arr[size-1-i];
			arr[size-1-i]=temp;
                    }
                for(int i=0;i<arr.length;i++) {
                	System.out.print(arr[i] +" ");
		}
        }
}
