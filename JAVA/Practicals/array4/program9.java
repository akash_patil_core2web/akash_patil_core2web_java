//  Take a number from a user and store each element in an array by increasing the element by 1

import java.io.*;
class program9 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the size of number ");
		int size = Integer.parseInt(br.readLine());
         	
                int arr[] = new int[size];
                
                System.out.print("Enter a number :");
		int num=Integer.parseInt(br.readLine());
	
                int num1=0;
			while(num!=0) {
				int digit=num%10;
				
				for(int j=num1;j<size;j++) {
					arr[j]=digit+1;
					
					num1++;
					
					break;
				}

				num/=10;
               		 }
                	
		
		
                for(int i=size-1;i>=0;i--) {
			System.out.print((arr[i]) +",");
		}
	}
}
