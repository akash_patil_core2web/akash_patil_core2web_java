//  Print the factorial of each element in array

import java.io.*;
class program10 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter the size of array: ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.print("Enter array element :");
		for(int i=0;i<size;i++) {
                	arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0;i<size;i++) {
                int num = arr[i];
		int fact=1;
                        while(num>=1) {
                                fact=num*fact;
				num--;
			}
			System.out.print(fact +",");

		}
        }
}
