//find the difference between maximum element in an array and minimum element in an array
import java.io.*;
class program2 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                 int max = arr[0];
		 int min = arr[0];
                 for (int i = 1; i<size; i++) {
                        if(arr[i]>max) {
				max=arr[i];
			}
			if(arr[i]<min) {
				min= arr[i];
			}
                    }
                System.out.println("Difference between max and min element is : "+(max-min));
        }
}
