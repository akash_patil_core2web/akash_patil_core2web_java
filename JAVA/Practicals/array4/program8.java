// Find the Second minimum element in an array
import java.io.*;
class program8 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");

                for(int i=0;i<size;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                int min1=arr[0];
                for(int i=1;i<size/2;i++) {
                        if(arr[i]<min1) {
				min1=arr[i];
			}
                }
                int min2=arr[0];
                for (int i = 1; i < arr.length; i++) {
                        if(arr[i]<=min2 && arr[i]>min1) {
				min2=arr[i];
			}
                }
		System.out.println("Second lowest element in an array is :"+min2);
        }
}
