import java.io.*;
class program9 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                
                for(int i=1;i<=row;i++) {
			int num=1;

                        for(int j=row;j>=i;j--) {
                                System.out.print(num++ +" ");
                        }
                        
                        System.out.println();

                }
        }
}
