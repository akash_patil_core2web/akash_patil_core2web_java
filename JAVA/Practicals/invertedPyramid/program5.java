/* row = 3
 *
 * A A A A A
 *   B B B
 *     C
 *
 * row = 4
 *
 * A A A A A A A
 *   B B B B B
 *     C C C
 *       D      */

import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
                char ch ='A';
                for(int i=1;i<=row;i++) {
                        for(int sp=1;sp<i;sp++) {
                                System.out.print("\t");
                        }
                        for(int j=1;j<=(row-i)*2+1;j++) {
                                System.out.print(ch +"\t");
                        }
                        ch++;
                        System.out.println();
                }
        }
}
