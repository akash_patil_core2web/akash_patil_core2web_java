/* row = 3
 *
 * 3 2 1 2 3
 *   2 1 2
 *     1
 *
 * row = 4
 *
 * 4 3 2 1 2 3 4
 *   3 2 1 2 3
 *     2 1 2
 *       1      */

import java.io.*;
class program10 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
                int num =row;
                for(int i=row;i>=1;i--) {
                        for(int sp=row;sp>i;sp--) {
                                System.out.print("\t");
                        }

                        for(int j=1;j<=i*2-1;j++) {
                                if(j<i) {
                                        System.out.print(num +"\t");
                                        num--;
                                }
                                else {
                                        System.out.print(num +"\t");
                                        num++;

                                        }
                                 }
                                num=i-1;
                                System.out.println();
                        }
                }
}
