/* row = 3
 *
 * 3 3 3 3 3
 *   2 2 2
 *     1
 *
 * row = 4
 *
 * 4 4 4 4 4 4 4
 *   3 3 3 3 3
 *     2 2 2
 *       1      */

import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
		int num =row;
                for(int i=1;i<=row;i++) {
                        for(int sp=1;sp<i;sp++) {
                                System.out.print("\t");
                        }
                        for(int j=1;j<=(row-i)*2+1;j++) {
                                System.out.print(num +"\t");
                        }
			num--;
                        System.out.println();
                }
        }
}
