/* row = 3
 *
 * C C C C C
 *   B B B
 *     A
 *
 * row = 4
 *
 * D D D D D D D
 *   C C C C C
 *     B B B
 *       A      */

import java.io.*;
class program6 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows : ");
                int row = Integer.parseInt(br.readLine());
                int num =row+64;
                for(int i=1;i<=row;i++) {
                        for(int sp=1;sp<i;sp++) {
                                System.out.print("\t");
                        }
                        for(int j=1;j<=(row-i)*2+1;j++) {
                                System.out.print((char)num +"\t");
                        }
                        num--;
                        System.out.println();
                }
        }
}
