/* row = 3
 *     C
 *   B B B
 * A A A A A
 *   B B B
 *     C
 * ROW = 4
 *       D
 *     C C C
 *   B B B B B
 * A A A A A A A
 *   B B B B B
 *     C C C
 *       D	*/
import java.io.*;
class program9 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
                int num=row+64;
                int s=0;
                int p=0;
                for(int i=1;i<row*2;i++) {
                        int num1=(i-row+1)+64;
                        if(i<=row) {
                                s=row-i;
                                p=i*2-1;
                        }
                        else {
                                s=i-row;
                                p-=2;
                        }
                        for(int j=1;j<=s;j++) {
                                System.out.print("\t");
                        }
                        for(int j=1;j<=p;j++) {
                                if(i<=row) {
                                        System.out.print((char)num +"\t");
                                }
                                else {
                                        System.out.print((char)num1 +"\t");
                                }
                        }
                        num--;
                        System.out.println();
                }
        }
}
