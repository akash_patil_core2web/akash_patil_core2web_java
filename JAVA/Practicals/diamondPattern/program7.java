/*ROW = 3
 *     A
 *   A B A
 * A B C B A
 *   A B A
 *     A
 * row = 4
 *       A
 *     A B A
 *   A B C B A
 * A B C D C B A
 *   A B C B A
 *     A B A
 *       A	*/
import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int s=0;
                int p=0;
                for(int i=1;i<row*2;i++) {

                        int num=65;
                        if(i<=row) {
                                s=row-i;
                                p=i*2-1;
                        }
                        else {
                                s=i-row;
                                p-=2;
                        }
                        for(int j=1;j<=s;j++) {
                                System.out.print("\t");
                        }
                        for(int j=1;j<=p;j++) {
                                if(i<=row) {
                                        if(j<i) {
                                                System.out.print((char)num +"\t");
						num++;
                                        }
                                        else {
                                                System.out.print((char)num +"\t");
						num--;
                                        }
                                }
                                else {
                                        if(j<row*2-i) {
                                                System.out.print((char)num +"\t");
						num++;
                                        }
                                        else {
                                                System.out.print((char)num +"\t");
						num--;
                                        }
                                }
                        }
			System.out.println();
		}
	}
}
