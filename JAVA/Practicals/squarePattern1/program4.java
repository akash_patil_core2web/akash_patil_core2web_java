/* row = 3
 *
 * &  &  &
 * &  $  &
 * &  &  &
 *
 * Row = 4
 * &  &  &  &
 * &  $  &  $
 * &  &  &  &
 * &  $  &  $     */

import java.io.*;
class program4 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(i%2==1) {
                                        System.out.print("& \t ");

                                }
                                else {
					if(j%2==1) {
                                        	System.out.print("& \t ");
					}
					else {
                                        	System.out.print("$ \t ");
					}
                                }
                              
                        }
                        System.out.println();
                }
        }
}
