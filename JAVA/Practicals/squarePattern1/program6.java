/* row = 3
 *
 * 9  8  7
 * 9  4  4
 * 9  8  7
 *
 * Row = 4
 * 16 15  14 13
 * 16 11  11 6
 * 16 15  14 13
 * 16 11  11 6    */

import java.io.*;
class program6 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                
                for(int i=1;i<=row;i++) {
			int num=row*row;
                        for(int j=1;j<=row;j++) {
                                if(i%2==1) {
                                        System.out.print(num-- +" ");
                                }
                                else {
					if(j%2==1) {
                                        	System.out.print(num +" ");
						num-=5;
					}
					else {
						System.out.print(num +" ");
					}
                                }
                                
                        }
                        System.out.println();
                }
        }
}

