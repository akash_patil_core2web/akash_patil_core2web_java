/* row = 3
 *
 * 3  $  4
 * 4  4  4
 * 4  $  5
 *
 * Row = 4
 * 4  $  5  $
 * 6  6  6  6
 * 6  $  7  $
 * 8  8  8  8     */

import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
		
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(i%2==1) {
					if(j%2==1) {
                                        	System.out.print(num +" ");
						num++;
					}
					else{
                                        	System.out.print("$ ");
					}
					
                                }
                                else {
                                        System.out.print(num +" ");
					
                           	}
                               
                        }
                        System.out.println();
                }
        }
}
