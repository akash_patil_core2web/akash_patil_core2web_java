/* row = 3
 *
 * C  d  e
 * F  g  h
 * I  j  k
 *
 * Row = 4
 * D  e  f  g
 * H  i  j  k
 * L  m  n  o
 * P  q  r  s     */

import java.io.*;
class program1 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                int ch1=64+row;
		int ch2=96+row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(j==1) {
                                        System.out.print((char)ch1 +" ");
					
                                }
                                else {
                                        System.out.print((char)ch2 +" ");
                                }
			ch1++;
			ch2++;	
                        }
                        System.out.println();
                }
        }
}
