/* row = 3
 *
 * 9  @   7
 * 12 @   8
 * 9  @   3
 *
 * Row = 4
 * 16  @  14  @
 * 64  @  20  @
 * 143 @  18  @
 * 256 @  8   @    */

import java.io.*;
class program9 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num1=row*row;
		int num2=row*row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                               if(j==1) {
                                        System.out.print(num2*i +" ");
                                }
                                else if(j%2==0) {
                                	System.out.print("@ ");
                                }
                                else {
					System.out.print(num1 +" ");
				}
				num2--;
                                num1=num1-i;       

                        }
			num1=num2*(i+1);
                        System.out.println();
                }
	}
}
