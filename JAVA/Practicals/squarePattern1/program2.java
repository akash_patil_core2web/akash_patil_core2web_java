/* row = 3
 *
 * c  d  E
 * f  G  H
 * I  J  K
 *
 * Row = 4
 * d  e  f  G
 * h  i  J  K
 * L  M  N  O
 * P  Q  R  S     */

import java.io.*;
class program2 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(j<=row-i) {
                                        System.out.print((char)(num+96) +" ");

                                }
                                else {
                                        System.out.print((char)(num+64) +" ");
                                }
				num++;
                        }
                        System.out.println();
                }
        }
}
