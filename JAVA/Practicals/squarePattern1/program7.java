/* row = 3
 *
 * 8  16  24
 * 36 49  64
 * 80 100 120
 *
 * Row = 4
 * 15  25  35  49
 * 64  81  100 121
 * 143 169 195 225
 * 256 289 324 361    */

import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                               if(i%2==0) {
                                        System.out.print(num*num +" ");
                                }
                                else {
                                        if(j%2==0) {
                                                System.out.print(num*num +" ");
                                                
                                        }
                                        else {
                                                System.out.print((num*num) -1+" ");
                                        }
                                }
				num++;

                        }
                        System.out.println();
                }
        }
}	
