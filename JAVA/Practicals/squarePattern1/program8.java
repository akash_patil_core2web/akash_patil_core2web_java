/* row = 3
 *
 * 8  e  24
 * g  h  i
 * 80 k  120
 *
 * Row = 4
 * 15  f   35  h
 * i   j   k   l
 * 143 n   195 p
 * q   r   s   t    */

import java.io.*;
class program8 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num1=row+1;
		int num2=row-1;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                               if(i%2==1) {
				       if(j%2==1) {
                                        	System.out.print(num1*num2 +" ");
				       }
				       else {
                                        	System.out.print((char)(num1+96) +" ");

                                	}
			       }
                                else {
                                          System.out.print((char)(num1+96) +" ");
                                }
                                num1++;
				num2++;
                        }
                        System.out.println();
                }
	}
}
