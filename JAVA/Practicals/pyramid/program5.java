/* row = 3
 *
 * 	  1
 * 	1 2 1
 *    1 2 3 2 1 
 *
 * row = 4
 *
 *  	  1
 *  	1 2 1
 *    1 2 3 2 3
 *  1 2 3 4 3 2 1	*/


import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++) {
                                System.out.print("  ");
                        }
                        int num=1;
                        for(int j=1;j<=i*2-1;j++) {
                               if(j<i)  {
				       System.out.print(num +" ");
				       num++;
			       }
			       else {
				       System.out.print(num +" ");
				       num--;
			       }
                        }
                        System.out.println();
                }
        }
}
