/* row =3
 *
 * 	  1
 * 	B B B
 *    3 3 3 3 3
 *
 *  ROW =4
 *
 *  	   1
 *       B B B 
 *     3 3 3 3 3
 *   D D D D D D D	*/

import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		char num ='A';
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++) {
                                System.out.print("  ");
                        }
                        
                        for(int j=1;j<=i*2-1;j++) {
				if(i%2==1) {

                               		System.out.print(i +" ");
                      		  }
				else {
                                        System.out.print((char)(64+i) +" ");
				}
				
                       		
               		 }
			System.out.println();
       		 }
	}
}
