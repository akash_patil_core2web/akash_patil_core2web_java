/* row = 3
 *
 * 	   C
 * 	 B C B
 *     A B C B A
 *
 * ROW = 4
 *
 * 	    D
 * 	  C D C
 * 	B C D C B
 *    A B C D C B A	*/

import java.io.*;
class program10 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
	
		int num=64+row;
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++) {
                                System.out.print("  ");
                        }
                    

                        for(int j=1;j<=i*2-1;j++) {
                             
                                        if(j<i) {
                                                System.out.print((char)num +" ");
                                                num++;
                                        }
                                        else {
                                                System.out.print((char)num +" ");
                                                num--;
                                        }
                                                                                                                                                                                    }
                        
			System.out.println();
                }
        }
}
