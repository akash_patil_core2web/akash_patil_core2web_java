/* row = 3
 *
 *          A
 *        a b a                                                                                                                                              *      A B C B A 
 *
 *
 * row = 4
 * 	    A
 *        a b a
 *      A B C B A
 *    a b c d c b a 	*/



import java.io.*;
class program9 {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no of rows :");
		int row = Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++) {
			for(int space=1;space<=row-i;space++) {
				System.out.print("  ");
			}
			char ch1='A';
			char ch2='a';

			for(int j=1;j<=i*2-1;j++) {
				if(i%2==1) {
					if(j<i) {
						System.out.print(ch1 +" ");
						ch1++;
					}
					else {
						System.out.print(ch1 +" ");
						ch1--;
					}
				}
				else {
                                        if(j<i) {
                                                System.out.print(ch2 +" ");
                                                ch2++;                                                                                                                                               }                                                                                                                                                           else {
                                                System.out.print(ch2 +" ");
                                                ch2--;                                                                                                                                               }
                                }
			}
			System.out.println();
		}
	}
}


