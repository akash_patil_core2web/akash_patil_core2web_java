//Print the count of digits in elements of array
import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int [size];
                System.out.print("Enter elements in an array :");

                for(int i=0;i<size;i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                //int num=arr[i];
                for(int i=0;i<size;i++) {
                	int num=arr[i];
			int count=0;
                                while(num!=0) {
                                	count++;
					num/=10;
                                }
                        System.out.print(count +",");
                }
        }
}
