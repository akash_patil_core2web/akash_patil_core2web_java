//Check the first duplicate element in an array and return its index
import java.io.*;
class program4 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int [size];
                System.out.print("Enter elements in an array :");

                for(int i=0;i<size;i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int flag=0;
                for(int i=0;i<size;i++) {
			for(int j=0;j<size-1;j++) {
				if(arr[i]==arr[j+1]) {
                                System.out.print("First duplicate index present at index "+i);
				flag=1;
				break;
				}
				
			}
			if(flag==1) {
				break;
			}
		}
        }
}
