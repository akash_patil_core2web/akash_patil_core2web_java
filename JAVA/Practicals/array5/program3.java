// Check if an array is a palindrome or not
import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int [size];
                System.out.print("Enter elements in an array :");

                for(int i=0;i<size;i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int flag=0;
                int F=0;
		int R=size-1;

                for(int i=0;i<size/2;i++) {
                        if(arr[F]!=arr[R]) {
                                flag=1;
				break;
                        }
                        F++;
			R--;
                }
                if(flag==1) {
                        System.out.print("The given array is not pallindrome..");
                }
                else {
                        System.out.print("The given array is pallindrome..");
                }
        }
}
