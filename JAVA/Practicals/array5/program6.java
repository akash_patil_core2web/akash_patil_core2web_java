import java.io.*;
class program6 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
		int index=0;
                for(int i=0;i<arr.length;i++) {
                  int num = arr[i];
                  
                  int count = 0;
                        for (int j = 2; j <= (num / 2); j++) {
                                if (num % j == 0) {
                                 count++;
                                 break;
                                }
                         }
                         if (count == 0) {
                                 index=i;
                                 break;
                         }
                }
		System.out.print("First prime no found at index : "+index);
        }
}
