// Print the sum of odd and even numbers in an array
import java.io.*;
class program2 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int [size];
                System.out.print("Enter elements in an array :");

                for(int i=0;i<size;i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int sum1=0;
                int sum2=0;

                for(int i=0;i<size;i++) {
                        if(arr[i]%2==0) {
                                sum1=sum1+arr[i];
                        }
                        else {
                                sum2=sum2+arr[i];
                        }
                       
                }
                
                System.out.println("Sum of even elements : "+sum1);
                System.out.println("Sum of odd elements : "+sum2);
                
        }
}
