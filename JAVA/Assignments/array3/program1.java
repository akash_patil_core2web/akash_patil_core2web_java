// Write a program to add 15 in all elements of the array and print it 
//
import java.io.*;
class program1 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

		System.out.print("Elements after adding 15 :");
		for(int i=0;i<size;i++) {
			arr[i]=arr[i]+15;
			System.out.print(arr[i]+" ");
		}
	}
}
