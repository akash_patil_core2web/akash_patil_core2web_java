// Write a program to print the product of prime elements in an array

import java.io.*;
class program10 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
		int prod = 1;
                for(int i=0;i<arr.length;i++) {
                  int num = arr[i];
		  int count = 0;
                        for (int j = 2; j <= (num / 2); j++) {
                                if (num % j == 0) {
                                 count++;
                                 break;
                                }
                         }
                         if (count == 0) {
				 prod = prod*arr[i];
                         }
                }
                System.out.println("Product of array element is "+prod+ " ");
        }
}
