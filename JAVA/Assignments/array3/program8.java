//Print the composite number in array

import java.io.*;
class program8 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.print("Enter array elements :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                for(int i=0;i<arr.length;i++) {
                  int num = arr[i];
                  int count = 0;
                        for (int j = 2; j <= (num / 2); j++) {
                                if (num % j == 0) {
                                 count++;
                                 break;
                                }
                         }
                         if (count == 1) {
                                 System.out.print(arr[i] + " ");
                         }
                }
        }
}
