import java.io.*;
class program7 {
        public static void main(String args[])throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of an array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter elements in array :");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }


                for(int i=0;i<arr.length;i++) {
                        if(arr[i]%4==0) {
                                System.out.println(arr[i]+" is divisible by 4 and its index is "+i);
                        }
                }

        }

}
