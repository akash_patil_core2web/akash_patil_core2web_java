import java.io.*;
class program9 {
        public static void main(String args[])throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of an array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter elements in array :");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }


                for(int i=0;i<arr.length;i++) {
                        if(i%2==1) {
                                System.out.println(arr[i]+" is an odd indexed element");
                        }
                }

        }

}
