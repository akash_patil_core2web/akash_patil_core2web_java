import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an array :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

                System.out.println("Enter elements in array :");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("elements in array is :");
                for(int i=0;i<arr.length;i++) {
			if(arr[i]%2==0) {
                        	System.out.println(arr[i]);
			}
                }
        }
}
