import java.io.*;
class program9 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter array element :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }


                System.out.println();
		
		int min = arr[0];
                for(int i=1;i<arr.length;i++) {
                        if(arr[i]<min) {
                                min = arr[i];
                         }
                 }
		System.out.println("Minimum number in the array is : "+min);
        }
}
