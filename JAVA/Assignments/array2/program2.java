import java.io.*;
class program2 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter array element :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                       
                }
		System.out.print("Elements divisible by 3 :");
			int sum = 0;
                for(int i=0;i<arr.length;i++) {
                        if(arr[i]%3==0) {
			  	System.out.print(arr[i] +",");
				sum+=arr[i];
                        }
                }
		System.out.println();

		System.out.println("Sum of elements divisible by 3 : "+sum);
        }
}
