import java.io.*;
class program5 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of Array :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter array element :");


                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                
                int sum =0;

                for(int i=0;i<arr.length;i++) {
                        if(i%2==1) {
				sum+=arr[i];
                         }
                }
                System.out.println("Sum of odd index element is : "+sum);
        }
}
