import java.util.*;
import java.util.stream.Collectors;
class Solution {
    public static int solve(int N, List<Integer> A, int Q, List<List<Integer>> B) {
        int count = 0; // Initialize count to keep track of occurrences of 200
        
        // Process queries
        for (List<Integer> query : B) {
            for (int value : query) {
                if (value == 200) {
                    count++; // Increment count for each occurrence of 200
                }
            }
        }
        
        return count; // Return the count of occurrences of 200
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int N = Integer.parseInt(scan.nextLine().trim());
        List<Integer> A = new ArrayList<>();
        for (int j = 0; j < N; j++) {
            A.add(Integer.parseInt(scan.nextLine().trim()));
        }

        int Q = Integer.parseInt(scan.nextLine().trim());
        List<List<Integer>> B = new ArrayList<>();
        for (int i = 0; i < Q; i++) {
            B.add(Arrays.asList(scan.nextLine().trim().split(" ")).stream().map(Integer::parseInt).collect(Collectors.toList()));
        }

        int result = solve(N, A, Q, B);
        System.out.println(result); // Output the count of occurrences of 200
    }
}

