/* 1
 * B C
 * 1 2 3
 * G H  I J	*/

import java.util.*;
class program11 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter No of rows :");
		int row = sc.nextInt();
		
		char ch='A';
		for(int i=1;i<=row;i++) {
			for(int j=1;j<=i;j++) {
				if(i%2==1) {
					System.out.print(j+" ");
					ch++;
				}
				else {
					System.out.print(ch++ +" ");
				}
			}
			System.out.println();
		}
	}
}


