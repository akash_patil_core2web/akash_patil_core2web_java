// Write a program to sort the array in ascending order
import java.io.*;
class program10 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;
                        System.out.print("Enter size of array :");
                        int size = Integer.parseInt(br.readLine());
                        int arr[] = new int[size];
                        System.out.print("Enter array elements :");
                        for(int i=0;i<size;i++) {
                                arr[i]=Integer.parseInt(br.readLine());
                        }

                        int temp=0;
                        for(int i=0;i<size;i++) {
                                for(int j=i+1;j<size;j++) {
                                        if(arr[i]>arr[j]) {
                                                temp=arr[i];
                                                arr[i]=arr[j];
                                                arr[j]=temp;
                                        }
                                }
                        }
			System.out.println("Sorted array is : ");
			for(int i=0;i<arr.length;i++) {
                        	System.out.println(arr[i]);
			}
        }
}
