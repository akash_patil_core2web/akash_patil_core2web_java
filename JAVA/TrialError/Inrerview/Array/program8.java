// Write a program to create an array of 'n' integer elements.
// Whwther n values should be taken from the user
// Insert the values from users and print the sum of all elements in an array
import java.io.*;
class program8 {
        public static void main(String []args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.print("Enter array elements : ");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                
                for(int i=0;i<arr.length;i++) {
			int digit=0;
			for(int j=0;j<arr.length;j++) {
				if(arr[i]==arr[j]) {
					digit++;
				}
			}
                	System.out.println("Frequency of "+arr[i]+" is : "+digit);       
                }
        }
}
