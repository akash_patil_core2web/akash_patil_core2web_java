// Write a program to create an array of 'n' integer elements.
// Whwther n values should be taken from the user
// Insert the values from users and print the sum of all elements in an array
import java.io.*;
class program7 {
        public static void main(String []args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.print("Enter array elements : ");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                int sum=0;
                for(int i=0;i<arr.length;i++) {
                        sum = sum+arr[i];
                }
                System.out.println("Sum of element in an array is : "+sum);
        }
}
