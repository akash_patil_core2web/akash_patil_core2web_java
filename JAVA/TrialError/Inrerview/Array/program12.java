// Find the common element between 2 arrays..
import java.io.*;
class program12 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;
                        System.out.print("Enter size of array :");
                        int size = Integer.parseInt(br.readLine());
                        int arr1[] = new int[size];
                        int arr2[] = new int[size];
                        System.out.print("Enter first array elements :");
                        for(int i=0;i<size;i++) {
                                arr1[i]=Integer.parseInt(br.readLine());
                                arr2[i]=Integer.parseInt(br.readLine());
                        }
                        
			for(int i=0;i<size;i++) {
				for(int j=0;j<size;j++) {
					if(arr1[i]==arr2[j]) {
						System.out.print(arr1[i]+" ");
					}
				}
			}
	}
}
