// remove specific elements from an array
import java.io.*;
class program16 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;
                        System.out.print("Enter size of array :");
                        int size = Integer.parseInt(br.readLine());
                        int arr[] = new int[size];
                        System.out.print("Enter array elements :");
                        for(int i=0;i<size;i++) {
                                arr[i]=Integer.parseInt(br.readLine());
                        }
