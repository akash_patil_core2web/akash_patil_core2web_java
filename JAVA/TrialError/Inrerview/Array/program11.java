// Take character array as input,but only print characters do not print special characters
import java.io.*;
class program11 {
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of  array : ");
		int size = Integer.parseInt(br.readLine());
		System.out.print("Enter characters in array : ");
		char arr[]=new char[size];

		for(int i=0;i<arr.length;i++) {
			arr[i]= (char) br.read();
			br.skip(1);
		}
		for(int i=0;i<arr.length;i++) {
			int n = arr[i];
			if((n>=65 && n<=90) || (n>=97 && n<=122 )) {
				System.out.print(arr[i] +",");
			}
		}
	}
}
