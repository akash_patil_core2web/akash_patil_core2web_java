// Write a program to create an array of 'n' integer elements.
// Whwther n values should be taken from the user
// Insert the values from users and print even numbers from an array
import java.io.*;
class program4 {
        public static void main(String []args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.print("Enter array elements : ");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                for(int i=0;i<arr.length;i++) {
			if(arr[i]%2==0) {
                        	System.out.println(arr[i]);
			}
                }
        }
}
