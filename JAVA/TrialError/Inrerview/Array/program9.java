// Write a program to create an array of 'n' integer elements.
// Whwther n values should be taken from the user
// Insert the values from users and find the strong number from them
import java.io.*;
class program9 {
        public static void main(String []args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.print("Enter array elements : ");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }

                for(int i=0;i<arr.length;i++) {
                        int num = arr[i];
			int temp = num;
			int sum=0;
			
			while(num>0) {
				int digit=num%10;
				int mul=1;

				while(digit>=1) {
					mul=mul*digit;
					digit--;
				}
				num/=10;
				sum=sum+mul;
                        
                        }
			if(sum==temp) {
                        	System.out.println("Strong no is: "+temp);
			}
                }
        }
}
