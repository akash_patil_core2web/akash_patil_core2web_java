//Find the number of even and odd integers in a given array of integer
import java.io.*;
class program13 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;
                        System.out.print("Enter size of array :");
                        int size = Integer.parseInt(br.readLine());
                        int arr[] = new int[size];
                        System.out.print("Enter array elements :");
                        for(int i=0;i<size;i++) {
                                arr[i]=Integer.parseInt(br.readLine());
                        }
			int count1 = 0;
			int count2 = 0;

                        for(int i=0;i<size;i++) {
                                if(arr[i]%2==0) {
					count1++;
				}
				else {
					count2++;
				}
                        }
			System.out.println("count of even elements in an array is : "+count1);
			System.out.println("count of odd elements in an array is : "+count2);
	}
}

