// Write a program to create an array of 'n' integer elements.
// Whwther n values should be taken from the user
// Insert the values from users and print minimum numbers from an array
import java.io.*;
class program6 {
        public static void main(String []args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.print("Enter array elements : ");
                for(int i=0;i<arr.length;i++) {
                        arr[i] = Integer.parseInt(br.readLine());
                }
                int n=arr[0];
                for(int i=1;i<arr.length;i++) {
                        if(arr[i]<=n) {
                                n=arr[i];
                        }
                }
                System.out.println("Maximum element in an array is : "+n);
        }
}
