import java.util.*;
import java.util.stream.Collectors;

class solution {
    public static int solve(int N, List<Integer> A, int Q, List<List<Integer>> B) {
        int result = 0;
        
        for (List<Integer> query : B) {
            for (int value : query) {
                if (value == 200) {
                    result += 200;
                } else {
                    // Assuming you perform some operation based on T, L, R values
                    // You can add your logic here
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int N = Integer.parseInt(scan.nextLine().trim());
        List<Integer> A = new ArrayList<>();
        for (int j = 0; j < N; j++) {
            A.add(Integer.parseInt(scan.nextLine().trim()));
        }

        int Q = Integer.parseInt(scan.nextLine().trim());
        List<List<Integer>> B = new ArrayList<>();
        for (int i = 0; i < Q; i++) {
            B.add(Arrays.asList(scan.nextLine().trim().split(" ")).stream().map(Integer::parseInt).collect(Collectors.toList()));
        }

        int result = solve(N, A, Q, B);
        System.out.println(result);
    }
}
