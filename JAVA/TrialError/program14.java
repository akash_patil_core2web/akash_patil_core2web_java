/* A
 * B C
 * C D E
 * D E F G
 * */

import java.util.*;
class program14 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no of rows : ");
		int row=sc.nextInt();
		
		for(int i=1;i<=row;i++) {
			int ch = 64+i;
			for(int j=1;j<=i;j++) {
				System.out.print((char)ch++ +" ");
			}
			System.out.println();
		}
	}
}
