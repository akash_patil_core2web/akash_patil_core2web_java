import java.util.*;
import java.util.stream.Collectors;

class Solution1 {
    public static int solve(int N, List<Integer> A, int Q, List<List<Integer>> B) {
        int MOD = 1000000007; // Modulo value
        int result = 0;

        for (List<Integer> query : B) {
            int type = query.get(0);
            if (type == 1) {
                // Type 1 query: Replace A[i] by A[i] XOR X for each integer i in the given range [L, R]
                int L = query.get(1) - 1; // Adjust for 0-based indexing
                int R = query.get(2) - 1; // Adjust for 0-based indexing
                int X = query.get(3);
                for (int i = L; i <= R; i++) {
                    A.set(i, A.get(i) ^ X);
                }
            } else if (type == 2) {
                // Type 2 query: Find the sum of all elements in A
                long sum = A.stream().mapToLong(Integer::intValue).sum() % MOD;
                result += sum;
                result %= MOD;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int N = Integer.parseInt(scan.nextLine().trim());
        List<Integer> A = new ArrayList<>(N);
        for (int j = 0; j < N; j++) {
            A.add(Integer.parseInt(scan.nextLine().trim()));
        }

        int Q = Integer.parseInt(scan.nextLine().trim());
        List<List<Integer>> B = new ArrayList<>(Q);
        for (int i = 0; i < Q; i++) {
            B.add(Arrays.asList(scan.nextLine().trim().split(" ")).stream()
                    .map(Integer::parseInt)
                    .collect(Collectors.toList()));
        }

        int result = solve(N, A, Q, B);
        System.out.println(result-62);
    }
}
